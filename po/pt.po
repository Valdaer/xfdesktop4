# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# Xfce Bot <transifex@xfce.org>, 2019
# Hugo Carvalho <hugokarvalho@hotmail.com>, 2019
# José Vieira <jvieira33@sapo.pt>, 2019
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-10-21 00:36+0200\n"
"PO-Revision-Date: 2019-10-20 16:40+0000\n"
"Last-Translator: José Vieira <jvieira33@sapo.pt>, 2019\n"
"Language-Team: Portuguese (https://www.transifex.com/xfce/teams/16840/pt/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: pt\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ../settings/main.c:419 ../src/xfdesktop-special-file-icon.c:292
#: ../src/xfdesktop-special-file-icon.c:478
msgid "Home"
msgstr "Pasta pessoal"

#: ../settings/main.c:421
msgid "Filesystem"
msgstr "Sistema de ficheiros"

#: ../settings/main.c:423 ../src/xfdesktop-special-file-icon.c:296
msgid "Trash"
msgstr "Lixo"

#: ../settings/main.c:425
msgid "Removable Devices"
msgstr "Dispositivos amovíveis"

#: ../settings/main.c:427
msgid "Network Shares"
msgstr "Partilhas de rede"

#: ../settings/main.c:429
msgid "Disks and Drives"
msgstr "Discos e unidades"

#: ../settings/main.c:431
msgid "Other Devices"
msgstr "Outros dispositivos"

#. Display the file name, file type, and file size in the tooltip.
#: ../settings/main.c:545
#, c-format
msgid ""
"<b>%s</b>\n"
"Type: %s\n"
"Size: %s"
msgstr ""
"<b>%s</b>\n"
"Tipo: %s\n"
"Tamanho: %s"

#: ../settings/main.c:732
#, c-format
msgid "Wallpaper for Monitor %d (%s)"
msgstr "Papel de parede do ecrã %d (%s)"

#: ../settings/main.c:735
#, c-format
msgid "Wallpaper for Monitor %d"
msgstr "Papel de parede do ecrã %d"

#: ../settings/main.c:741
msgid "Move this dialog to the display you want to edit the settings for."
msgstr "Mova este diálogo para o ecrã no qual pretende editar as definições."

#: ../settings/main.c:748
#, c-format
msgid "Wallpaper for %s on Monitor %d (%s)"
msgstr "Papel de parede para %s no monitor %d (%s)"

#: ../settings/main.c:752
#, c-format
msgid "Wallpaper for %s on Monitor %d"
msgstr "Papel de parede para %s no monitor %d"

#: ../settings/main.c:759
msgid ""
"Move this dialog to the display and workspace you want to edit the settings "
"for."
msgstr ""
"Mova este diálogo para o ecrã e área de trabalho na qual pretende editar as "
"definições."

#. Single monitor and single workspace
#: ../settings/main.c:767
#, c-format
msgid "Wallpaper for my desktop"
msgstr "Papel de parede do ambiente de trabalho"

#. Single monitor and per workspace wallpaper
#: ../settings/main.c:773
#, c-format
msgid "Wallpaper for %s"
msgstr "Papel de parede para %s"

#: ../settings/main.c:778
msgid "Move this dialog to the workspace you want to edit the settings for."
msgstr ""
"Mova este diálogo para o ecrã e área de trabalho na qual pretende editar as "
"definições."

#: ../settings/main.c:1198
msgid "Image selection is unavailable while the image style is set to None."
msgstr ""
"A seleção de imagens não está disponível se o estilo de imagem for Nenhum."

#: ../settings/main.c:1575
msgid "Spanning screens"
msgstr "Expandir ecrãs"

#. TRANSLATORS: Please split the message in half with '\n' so the dialog will
#. not be too wide.
#: ../settings/main.c:1743
msgid ""
"Would you like to arrange all existing\n"
"icons according to the selected orientation?"
msgstr ""
"Deseja colocar todos os ícones\n"
"de acordo com a orientação selecionada?"

#: ../settings/main.c:1749
msgid "Arrange icons"
msgstr "Ordenar ícones"

#. printf is to be translator-friendly
#: ../settings/main.c:1757 ../src/xfdesktop-file-icon-manager.c:796
#: ../src/xfdesktop-file-icon-manager.c:1353
#, c-format
msgid "Unable to launch \"%s\":"
msgstr "Incapaz de iniciar \"%s\":"

#: ../settings/main.c:1758 ../src/xfdesktop-file-icon-manager.c:798
#: ../src/xfdesktop-file-icon-manager.c:1100
#: ../src/xfdesktop-file-icon-manager.c:1354 ../src/xfdesktop-file-utils.c:683
#: ../src/xfdesktop-file-utils.c:1217 ../src/xfdesktop-file-utils.c:1295
#: ../src/xfdesktop-file-utils.c:1319 ../src/xfdesktop-file-utils.c:1381
msgid "Launch Error"
msgstr "Erro do lançador"

#: ../settings/main.c:1760 ../settings/xfdesktop-settings-ui.glade.h:21
#: ../src/xfdesktop-file-icon-manager.c:560
#: ../src/xfdesktop-file-icon-manager.c:579
#: ../src/xfdesktop-file-icon-manager.c:664
#: ../src/xfdesktop-file-icon-manager.c:800
#: ../src/xfdesktop-file-icon-manager.c:1104
#: ../src/xfdesktop-file-icon-manager.c:1356
#: ../src/xfdesktop-file-icon-manager.c:2938 ../src/xfdesktop-file-utils.c:686
#: ../src/xfdesktop-file-utils.c:706 ../src/xfdesktop-file-utils.c:761
#: ../src/xfdesktop-file-utils.c:825 ../src/xfdesktop-file-utils.c:886
#: ../src/xfdesktop-file-utils.c:947 ../src/xfdesktop-file-utils.c:995
#: ../src/xfdesktop-file-utils.c:1050 ../src/xfdesktop-file-utils.c:1108
#: ../src/xfdesktop-file-utils.c:1160 ../src/xfdesktop-file-utils.c:1221
#: ../src/xfdesktop-file-utils.c:1297 ../src/xfdesktop-file-utils.c:1323
#: ../src/xfdesktop-file-utils.c:1385 ../src/xfdesktop-file-utils.c:1444
#: ../src/xfdesktop-file-utils.c:1460 ../src/xfdesktop-file-utils.c:1522
#: ../src/xfdesktop-file-utils.c:1540 ../src/xfdesktop-volume-icon.c:523
#: ../src/xfdesktop-volume-icon.c:569 ../src/xfdesktop-volume-icon.c:605
msgid "_Close"
msgstr "_Fechar"

#: ../settings/main.c:1915
msgid "Image files"
msgstr "Ficheiros de imagem"

#. Change the title of the file chooser dialog
#: ../settings/main.c:1923
msgid "Select a Directory"
msgstr "Selecione um diretório"

#: ../settings/main.c:2128
msgid "Settings manager socket"
msgstr "Socket do gestor de definições"

#: ../settings/main.c:2128
msgid "SOCKET ID"
msgstr "ID do SOCKET"

#: ../settings/main.c:2129
msgid "Version information"
msgstr "Informações de versão"

#: ../settings/main.c:2130 ../src/xfdesktop-application.c:842
msgid "Enable debug messages"
msgstr "Ligar mensagens de debug"

#: ../settings/main.c:2158
#, c-format
msgid "Type '%s --help' for usage."
msgstr "Digite '%s --help' para utilização."

#: ../settings/main.c:2170
msgid "The Xfce development team. All rights reserved."
msgstr "A equipa de desenvolvimento do Xfce. Todos os direitos reservados."

#: ../settings/main.c:2171
#, c-format
msgid "Please report bugs to <%s>."
msgstr "Por favor reporte os erros em <%s>."

#: ../settings/main.c:2178
msgid "Desktop Settings"
msgstr "Definições do ambiente de trabalho"

#: ../settings/main.c:2180
msgid "Unable to contact settings server"
msgstr "Incapaz de contactar o servidor de definições"

#: ../settings/main.c:2182
msgid "Quit"
msgstr "Sair"

#: ../settings/xfce-backdrop-settings.desktop.in.h:1
#: ../settings/xfdesktop-settings-ui.glade.h:18 ../src/xfce-desktop.c:1170
msgid "Desktop"
msgstr "Ambiente de trabalho"

#: ../settings/xfce-backdrop-settings.desktop.in.h:2
#: ../settings/xfdesktop-settings-ui.glade.h:19
msgid "Set desktop background and menu and icon behavior"
msgstr "Definir fundo da área de trabalho e comportamento do menu e ícones"

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:1
msgid "Solid color"
msgstr "Cor sólida"

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:2
msgid "Horizontal gradient"
msgstr "Gradiente horizontal"

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:3
msgid "Vertical gradient"
msgstr "Gradiente vertical"

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:4
msgid "Transparent"
msgstr "Transparente"

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:5
msgid "Choose the folder to select wallpapers from."
msgstr "Escolha a pasta para obtenção de imagens."

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:6
msgid "St_yle:"
msgstr "E_stilo:"

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:7
msgid "Specify how the image will be resized to fit the screen."
msgstr "Especifique o tipo de redimensionamento para ajuste ao ecrã."

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:8
#: ../settings/xfdesktop-settings-ui.glade.h:4
msgid "None"
msgstr "Nenhum"

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:9
msgid "Centered"
msgstr "Centrado"

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:10
msgid "Tiled"
msgstr "Mosaico"

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:11
msgid "Stretched"
msgstr "Esticado"

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:12
msgid "Scaled"
msgstr "Escala"

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:13
msgid "Zoomed"
msgstr "Ampliado"

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:14
msgid "Spanning Screens"
msgstr "Expandir ecrãs"

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:15
msgid "Specify the style of the color drawn behind the backdrop image."
msgstr "Especifique o estilo da cor por trás da imagem de fundo."

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:16
msgid "Specifies the solid color, or the \"left\" or \"top\" color of the gradient."
msgstr "Especifica a cor sólida, à \"esquerda\" ou \"superior\" do gradiente."

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:17
msgid "Select First Color"
msgstr "Selecione a primeira cor"

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:18
msgid "Specifies the \"right\" or \"bottom\" color of the gradient."
msgstr "Especifica a cor à \"direita\" ou \"inferior\" do gradiente."

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:19
msgid "Select Second Color"
msgstr "Selecione a segunda cor"

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:20
msgid "Apply to all _workspaces"
msgstr "Aplicar a todas as áreas de trabal_ho"

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:21
msgid "_Folder:"
msgstr "_Pasta:"

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:22
msgid "C_olor:"
msgstr "C_or:"

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:23
msgid "Change the _background "
msgstr "Mudar imag_em de fundo"

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:24
msgid ""
"Automatically select a different background from the current directory."
msgstr ""
"Selecionar automaticamente um fundo diferente a partir do atual diretório."

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:25
msgid "Specify how often the background will change."
msgstr "Especifique a frequência da alteração de imagem."

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:26
msgid "in seconds:"
msgstr "em segundos:"

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:27
msgid "in minutes:"
msgstr "em minutos:"

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:28
msgid "in hours:"
msgstr "em horas:"

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:29
msgid "at start up"
msgstr "ao arrancar"

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:30
msgid "every hour"
msgstr "todas as horas"

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:31
msgid "every day"
msgstr "todos os dias"

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:32
msgid "chronologically"
msgstr "cronologicamente"

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:33
msgid "Amount of time before a different background is selected."
msgstr "Intervalo de tempo entre a mudança de imagens de fundo."

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:34
msgid "_Random Order"
msgstr "Aleato_riamente"

#: ../settings/xfdesktop-settings-appearance-frame-ui.glade.h:35
msgid ""
"Randomly selects another image from the same directory when the wallpaper is"
" to cycle."
msgstr ""
"Seleciona uma imagem aleatoriamente do mesmo diretório quando o papel de "
"parede está em ciclo."

#: ../settings/xfdesktop-settings-ui.glade.h:1
msgid "Left"
msgstr "Esquerda"

#: ../settings/xfdesktop-settings-ui.glade.h:2
msgid "Middle"
msgstr "Centro"

#: ../settings/xfdesktop-settings-ui.glade.h:3
msgid "Right"
msgstr "Direita"

#: ../settings/xfdesktop-settings-ui.glade.h:5
msgid "Shift"
msgstr "Shift"

#: ../settings/xfdesktop-settings-ui.glade.h:6
msgid "Alt"
msgstr "Alt"

#: ../settings/xfdesktop-settings-ui.glade.h:7
msgid "Control"
msgstr "Controlo"

#: ../settings/xfdesktop-settings-ui.glade.h:8
msgid "Minimized application icons"
msgstr "Ícones das aplicações minimizadas"

#: ../settings/xfdesktop-settings-ui.glade.h:9
msgid "File/launcher icons"
msgstr "Ícones de ficheiro/lançador"

#: ../settings/xfdesktop-settings-ui.glade.h:10
msgid "Top Left Vertical"
msgstr "Topo esquerdo vertical"

#: ../settings/xfdesktop-settings-ui.glade.h:11
msgid "Top Left Horizontal"
msgstr "Topo esquerdo horizontal"

#: ../settings/xfdesktop-settings-ui.glade.h:12
msgid "Top Right Vertical"
msgstr "Topo direito vertical"

#: ../settings/xfdesktop-settings-ui.glade.h:13
msgid "Top Right Horizontal"
msgstr "Topo direito horizontal"

#: ../settings/xfdesktop-settings-ui.glade.h:14
msgid "Bottom Left Vertical"
msgstr "Fundo esquerda vertical"

#: ../settings/xfdesktop-settings-ui.glade.h:15
msgid "Bottom Left Horizontal"
msgstr "Fundo esquerda horizontal"

#: ../settings/xfdesktop-settings-ui.glade.h:16
msgid "Bottom Right Vertical"
msgstr "Fundo direita vertical"

#: ../settings/xfdesktop-settings-ui.glade.h:17
msgid "Bottom Right Horizontal"
msgstr "Fundo direita horizontal"

#: ../settings/xfdesktop-settings-ui.glade.h:20
msgid "_Help"
msgstr "_Ajuda"

#: ../settings/xfdesktop-settings-ui.glade.h:22
msgid "_Background"
msgstr "F_undo"

#: ../settings/xfdesktop-settings-ui.glade.h:23
msgid "Include applications menu on _desktop right click"
msgstr "Incluir aplicações no menu com clique _direito na área de trabalho"

#: ../settings/xfdesktop-settings-ui.glade.h:24
msgid "_Button:"
msgstr "_Botão:"

#: ../settings/xfdesktop-settings-ui.glade.h:25
msgid "Mo_difier:"
msgstr "Mo_dificador:"

#: ../settings/xfdesktop-settings-ui.glade.h:26
msgid "Show _application icons in menu"
msgstr "Mostrar ícon_es de aplicação no menu"

#: ../settings/xfdesktop-settings-ui.glade.h:27
msgid "_Edit desktop menu"
msgstr "_Editar menu do ambiente de trabalho"

#: ../settings/xfdesktop-settings-ui.glade.h:28
msgid "<b>Desktop Menu</b>"
msgstr "<b>Menu do ambiente de trabalho</b>"

#: ../settings/xfdesktop-settings-ui.glade.h:29
msgid "Show _window list menu on desktop middle click"
msgstr "Mostrar _lista de janelas com clique central no ambiente de trabalho"

#: ../settings/xfdesktop-settings-ui.glade.h:30
msgid "B_utton:"
msgstr "_Botão:"

#: ../settings/xfdesktop-settings-ui.glade.h:31
msgid "Modi_fier:"
msgstr "Modi_ficador:"

#: ../settings/xfdesktop-settings-ui.glade.h:32
msgid "Sh_ow application icons in menu"
msgstr "M_ostrar ícones de aplicação no menu"

#: ../settings/xfdesktop-settings-ui.glade.h:33
msgid "Show workspace _names in list"
msgstr "Mostrar _nomes das áreas de trabalho em lista"

#: ../settings/xfdesktop-settings-ui.glade.h:34
msgid "Use _submenus for the windows in each workspace"
msgstr "Utilizar _submenus para as janelas de cada área de trabalho"

#: ../settings/xfdesktop-settings-ui.glade.h:35
msgid "Show s_ticky windows only in active workspace"
msgstr "Só mostrar janelas fi_xas no ambiente de trabalho ativo"

#: ../settings/xfdesktop-settings-ui.glade.h:36
msgid "Show a_dd and remove workspace options in list"
msgstr "Mostrar opções de a_dição e remoção de área de trabalho em lista"

#: ../settings/xfdesktop-settings-ui.glade.h:37
msgid "<b>Window List Menu</b>"
msgstr "<b>Menu da lista de janelas</b>"

#: ../settings/xfdesktop-settings-ui.glade.h:38
msgid "_Menus"
msgstr "_Menus"

#: ../settings/xfdesktop-settings-ui.glade.h:39
msgid "Icon _type:"
msgstr "_Tipo de ícones:"

#: ../settings/xfdesktop-settings-ui.glade.h:40
msgid "Icon _size:"
msgstr "Taman_ho dos ícones:"

#: ../settings/xfdesktop-settings-ui.glade.h:41
msgid "48"
msgstr "48"

#: ../settings/xfdesktop-settings-ui.glade.h:42
msgid "Icons _orientation:"
msgstr "_Orientação de ícones:"

#: ../settings/xfdesktop-settings-ui.glade.h:43
msgid "Show icons on primary display"
msgstr "Mostrar ícones no ecrã principal"

#: ../settings/xfdesktop-settings-ui.glade.h:44
msgid "Use custom _font size:"
msgstr "Tamanho personali_zado de tipo de letra:"

#: ../settings/xfdesktop-settings-ui.glade.h:45
msgid "12"
msgstr "12"

#: ../settings/xfdesktop-settings-ui.glade.h:46
msgid "Show icon tooltips. Size:"
msgstr "Mostrar ajuda de ícones. Tamanho:"

#: ../settings/xfdesktop-settings-ui.glade.h:47
msgid "Size of the tooltip preview image."
msgstr "Tamanho da pré-visualização de imagem."

#: ../settings/xfdesktop-settings-ui.glade.h:48
msgid "128"
msgstr "128"

#: ../settings/xfdesktop-settings-ui.glade.h:49
msgid "Show t_humbnails"
msgstr "Mostrar mi_niaturas"

#: ../settings/xfdesktop-settings-ui.glade.h:50
msgid ""
"Select this option to display preview-able files on the desktop as "
"automatically generated thumbnail icons."
msgstr ""
"Selecione esta opção para pré-visualizar automaticamente os ficheiros no "
"ambiente de trabalho como miniatura."

#: ../settings/xfdesktop-settings-ui.glade.h:51
msgid "Show hidden files on the desktop"
msgstr "Mostrar ficheiros ocultos no ambiente de trabalho"

#: ../settings/xfdesktop-settings-ui.glade.h:52
msgid "Single _click to activate items"
msgstr "Um cliqu_e para ativar itens"

#: ../settings/xfdesktop-settings-ui.glade.h:53
msgid "<b>Appearance</b>"
msgstr "<b>Aparência</b>"

#: ../settings/xfdesktop-settings-ui.glade.h:54
msgid "<b>Default Icons</b>"
msgstr "<b>Ícones por omissão</b>"

#: ../settings/xfdesktop-settings-ui.glade.h:55
msgid "_Icons"
msgstr "Í_cones"

#: ../src/menu.c:93
msgid "_Applications"
msgstr "_Aplicações"

#: ../src/windowlist.c:73
#, c-format
msgid "Remove Workspace %d"
msgstr "Remover área de trabalho %d"

#: ../src/windowlist.c:74
#, c-format
msgid ""
"Do you really want to remove workspace %d?\n"
"Note: You are currently on workspace %d."
msgstr ""
"Deseja realmente remover a área de trabalho %d?\n"
"Nota: Encontra-se atualmente na área de trabalho %d."

#: ../src/windowlist.c:78
#, c-format
msgid "Remove Workspace '%s'"
msgstr "Remover área de trabalho '%s'"

#: ../src/windowlist.c:79
#, c-format
msgid ""
"Do you really want to remove workspace '%s'?\n"
"Note: You are currently on workspace '%s'."
msgstr ""
"Deseja realmente remover a área de trabalho '%s'?\n"
"Nota: Encontra-se atualmente na área de trabalho '%s'."

#. Popup a dialog box confirming that the user wants to remove a
#. * workspace
#: ../src/windowlist.c:86
msgid "Remove"
msgstr "Remover"

#: ../src/windowlist.c:272
msgid "Window List"
msgstr "Lista de janelas"

#: ../src/windowlist.c:297
#, c-format
msgid "<b>Workspace %d</b>"
msgstr "<b>Área de trabalho %d</b>"

#: ../src/windowlist.c:393 ../src/windowlist.c:395
msgid "_Add Workspace"
msgstr "_Adicionar área de trabalho"

#: ../src/windowlist.c:403
#, c-format
msgid "_Remove Workspace %d"
msgstr "_Remover área de trabalho %d"

#: ../src/windowlist.c:406
#, c-format
msgid "_Remove Workspace '%s'"
msgstr "_Remover área de trabalho \"%s\""

#: ../src/xfdesktop-application.c:834
msgid "Display version information"
msgstr "Mostrar informações da versão"

#: ../src/xfdesktop-application.c:835
msgid "Reload all settings"
msgstr "Recarregar todas as definições"

#: ../src/xfdesktop-application.c:836
msgid "Advance to the next wallpaper on the current workspace"
msgstr "Avançar para o próximo papel de parede na área de trabalho atual"

#: ../src/xfdesktop-application.c:837
msgid "Pop up the menu (at the current mouse position)"
msgstr "Mostrar menu (na posição atual do rato)"

#: ../src/xfdesktop-application.c:838
msgid "Pop up the window list (at the current mouse position)"
msgstr "Mostrar lista de janelas (na posição atual do rato)"

#: ../src/xfdesktop-application.c:840
msgid "Automatically arrange all the icons on the desktop"
msgstr "Dispor automaticamente os ícones no ecrã"

#: ../src/xfdesktop-application.c:843
msgid "Disable debug messages"
msgstr "Desligar mensagens de debug"

#: ../src/xfdesktop-application.c:844
msgid "Do not wait for a window manager on startup"
msgstr "Não esperar pelo gestor de janelas ao arrancar"

#: ../src/xfdesktop-application.c:845
msgid "Cause xfdesktop to quit"
msgstr "Obrigar ao fecho do xfdesktop"

#: ../src/xfdesktop-application.c:860
#, c-format
msgid "Failed to parse arguments: %s\n"
msgstr "Falha ao processar argumentos: %s\n"

#: ../src/xfdesktop-application.c:871
#, c-format
msgid "This is %s version %s, running on Xfce %s.\n"
msgstr "Este é o %s versão %s, executado no Xfce %s.\n"

#: ../src/xfdesktop-application.c:873
#, c-format
msgid "Built with GTK+ %d.%d.%d, linked with GTK+ %d.%d.%d."
msgstr "Criado com GTK+ %d.%d.%d, vinculado ao GTK+ %d.%d.%d."

#: ../src/xfdesktop-application.c:877
#, c-format
msgid "Build options:\n"
msgstr "Opções da versão:\n"

#: ../src/xfdesktop-application.c:878
#, c-format
msgid "    Desktop Menu:        %s\n"
msgstr "    Menu do ambiente de trabalho:        %s\n"

#: ../src/xfdesktop-application.c:880 ../src/xfdesktop-application.c:887
#: ../src/xfdesktop-application.c:894
msgid "enabled"
msgstr "ativo"

#: ../src/xfdesktop-application.c:882 ../src/xfdesktop-application.c:889
#: ../src/xfdesktop-application.c:896
msgid "disabled"
msgstr "inativo"

#: ../src/xfdesktop-application.c:885
#, c-format
msgid "    Desktop Icons:       %s\n"
msgstr "    Ícones do ambiente de trabalho:       %s\n"

#: ../src/xfdesktop-application.c:892
#, c-format
msgid "    Desktop File Icons:  %s\n"
msgstr "    Ícones do ficheiro do ambiente de trabalho:  %s\n"

#: ../src/xfdesktop-file-icon-manager.c:552
#: ../src/xfdesktop-file-icon-manager.c:570
#, c-format
msgid "Could not create the desktop folder \"%s\""
msgstr "Incapaz de criar a pasta do ambiente de trabalho \"%s\""

#: ../src/xfdesktop-file-icon-manager.c:557
#: ../src/xfdesktop-file-icon-manager.c:575
msgid "Desktop Folder Error"
msgstr "Erro na pasta do ambiente de trabalho"

#: ../src/xfdesktop-file-icon-manager.c:577
msgid ""
"A normal file with the same name already exists. Please delete or rename it."
msgstr "Já existe um ficheiro com este nome. Apague-o ou dê-lhe outro nome."

#: ../src/xfdesktop-file-icon-manager.c:661 ../src/xfdesktop-file-utils.c:757
#: ../src/xfdesktop-file-utils.c:821
msgid "Rename Error"
msgstr "Erro ao mudar de nome"

#: ../src/xfdesktop-file-icon-manager.c:662 ../src/xfdesktop-file-utils.c:822
msgid "The files could not be renamed"
msgstr "Os ficheiros não foram renomeados"

#: ../src/xfdesktop-file-icon-manager.c:663
msgid "None of the icons selected support being renamed."
msgstr "Nenhum dos ícones selecionados possuem suporte à renomeação."

#: ../src/xfdesktop-file-icon-manager.c:1025
#, c-format
msgid "_Open With \"%s\""
msgstr "_Abrir com \"%s\""

#: ../src/xfdesktop-file-icon-manager.c:1028
#, c-format
msgid "Open With \"%s\""
msgstr "Abrir com \"%s\""

#: ../src/xfdesktop-file-icon-manager.c:1102
msgid ""
"Unable to launch \"exo-desktop-item-edit\", which is required to create and "
"edit launchers and links on the desktop."
msgstr ""
"Incapaz de iniciar \"exo-desktop-item-edit\", necessário para criar e editar"
" lançadores e atalhos no ambiente de trabalho."

#: ../src/xfdesktop-file-icon-manager.c:1423
msgid "_Open all"
msgstr "_Abrir tudo"

#: ../src/xfdesktop-file-icon-manager.c:1437
msgid "_Open in New Window"
msgstr "_Abrir em nova janela"

#: ../src/xfdesktop-file-icon-manager.c:1439
#: ../src/xfdesktop-special-file-icon.c:545 ../src/xfdesktop-volume-icon.c:811
msgid "_Open"
msgstr "_Abrir"

#: ../src/xfdesktop-file-icon-manager.c:1459
msgid "Create _Launcher..."
msgstr "Criar _lançador..."

#: ../src/xfdesktop-file-icon-manager.c:1473
msgid "Create _URL Link..."
msgstr "Criar ligação _URL..."

#: ../src/xfdesktop-file-icon-manager.c:1487
msgid "Create _Folder..."
msgstr "Criar _pasta..."

#: ../src/xfdesktop-file-icon-manager.c:1499
msgid "Create _Document"
msgstr "Criar _Documento"

#: ../src/xfdesktop-file-icon-manager.c:1524
msgid "No templates installed"
msgstr "Nenhum modelo instalado"

#: ../src/xfdesktop-file-icon-manager.c:1540
msgid "_Empty File"
msgstr "_Ficheiro vazio"

#: ../src/xfdesktop-file-icon-manager.c:1555
msgid "_Execute"
msgstr "_Executar"

#: ../src/xfdesktop-file-icon-manager.c:1573
msgid "_Edit Launcher"
msgstr "_Editar lançador"

#: ../src/xfdesktop-file-icon-manager.c:1632
msgid "Open With"
msgstr "Abrir com"

#: ../src/xfdesktop-file-icon-manager.c:1659
#: ../src/xfdesktop-file-icon-manager.c:1673
msgid "Open With Other _Application..."
msgstr "Abrir com outra _aplicação..."

#: ../src/xfdesktop-file-icon-manager.c:1691
msgid "_Paste"
msgstr "_Colar"

#: ../src/xfdesktop-file-icon-manager.c:1709
msgid "Cu_t"
msgstr "Cor_tar"

#: ../src/xfdesktop-file-icon-manager.c:1721
msgid "_Copy"
msgstr "C_opiar"

#: ../src/xfdesktop-file-icon-manager.c:1733
msgid "Paste Into Folder"
msgstr "Colar dentro da pasta"

#: ../src/xfdesktop-file-icon-manager.c:1752
msgid "Mo_ve to Trash"
msgstr "Mo_ver para o Lixo"

#: ../src/xfdesktop-file-icon-manager.c:1764
msgid "_Delete"
msgstr "_Eliminar"

#: ../src/xfdesktop-file-icon-manager.c:1781
msgid "_Rename..."
msgstr "_Mudar nome..."

#: ../src/xfdesktop-file-icon-manager.c:1848
msgid "Arrange Desktop _Icons"
msgstr "Dispor í_cones do ambiente de trabalho"

#: ../src/xfdesktop-file-icon-manager.c:1858
msgid "_Next Background"
msgstr "_Fundo seguinte"

#: ../src/xfdesktop-file-icon-manager.c:1868
msgid "Desktop _Settings..."
msgstr "Definiçõe_s do ambiente de trabalho..."

#: ../src/xfdesktop-file-icon-manager.c:1882
#: ../src/xfdesktop-volume-icon.c:852
msgid "P_roperties..."
msgstr "_Propriedades..."

#: ../src/xfdesktop-file-icon-manager.c:2935
msgid "Load Error"
msgstr "Erro ao carregar"

#: ../src/xfdesktop-file-icon-manager.c:2937
msgid "Failed to load the desktop folder"
msgstr "Falha ao carregar a pasta de ambiente de trabalho"

#: ../src/xfdesktop-file-icon-manager.c:3503
msgid "Copy _Here"
msgstr "Copiar para _aqui"

#: ../src/xfdesktop-file-icon-manager.c:3503
msgid "_Move Here"
msgstr "_Mover para aqui"

#: ../src/xfdesktop-file-icon-manager.c:3503
msgid "_Link Here"
msgstr "Criar _Ligação aqui"

#: ../src/xfdesktop-file-icon-manager.c:3538
msgid "_Cancel"
msgstr "_Cancelar"

#. TRANSLATORS: file was modified less than one day ago
#: ../src/xfdesktop-file-utils.c:159
#, c-format
msgid "Today at %X"
msgstr "Hoje às %X"

#. TRANSLATORS: file was modified less than two days ago
#: ../src/xfdesktop-file-utils.c:163
#, c-format
msgid "Yesterday at %X"
msgstr "Ontem às %X"

#. Days from last week
#: ../src/xfdesktop-file-utils.c:168
#, c-format
msgid "%A at %X"
msgstr "%A às %X"

#. Any other date
#: ../src/xfdesktop-file-utils.c:171
#, c-format
msgid "%x at %X"
msgstr "%x às %X"

#. the file_time is invalid
#: ../src/xfdesktop-file-utils.c:181
msgid "Unknown"
msgstr "Desconhecido"

#: ../src/xfdesktop-file-utils.c:684
msgid "The folder could not be opened"
msgstr "Não foi possível abrir a pasta"

#: ../src/xfdesktop-file-utils.c:703
msgid "Error"
msgstr "Erro"

#: ../src/xfdesktop-file-utils.c:704
msgid "The requested operation could not be completed"
msgstr "A operação solicitada não foi terminada"

#: ../src/xfdesktop-file-utils.c:758
msgid "The file could not be renamed"
msgstr "O ficheiro não pode ser renomeado"

#: ../src/xfdesktop-file-utils.c:759 ../src/xfdesktop-file-utils.c:823
#: ../src/xfdesktop-file-utils.c:884 ../src/xfdesktop-file-utils.c:1048
#: ../src/xfdesktop-file-utils.c:1106 ../src/xfdesktop-file-utils.c:1158
#: ../src/xfdesktop-file-utils.c:1219 ../src/xfdesktop-file-utils.c:1321
#: ../src/xfdesktop-file-utils.c:1383 ../src/xfdesktop-file-utils.c:1458
#: ../src/xfdesktop-file-utils.c:1538
msgid ""
"This feature requires a file manager service to be present (such as the one "
"supplied by Thunar)."
msgstr ""
"Esta função requer um serviço do gestor de ficheiros (como aquele que é "
"disponibilizado pelo Thunar)."

#: ../src/xfdesktop-file-utils.c:882
msgid "Delete Error"
msgstr "Erro de eliminação"

#: ../src/xfdesktop-file-utils.c:883
msgid "The selected files could not be deleted"
msgstr "Os ficheiros selecionados não foram eliminados"

#: ../src/xfdesktop-file-utils.c:943 ../src/xfdesktop-file-utils.c:991
msgid "Trash Error"
msgstr "Erro no lixo"

#: ../src/xfdesktop-file-utils.c:944
msgid "The selected files could not be moved to the trash"
msgstr "Os ficheiros selecionados não foram movidos para o lixo"

#: ../src/xfdesktop-file-utils.c:945 ../src/xfdesktop-file-utils.c:993
msgid ""
"This feature requires a trash service to be present (such as the one "
"supplied by Thunar)."
msgstr ""
"Esta função requer um serviço de lixo (como aquele que é disponibilizado "
"pelo Thunar)."

#: ../src/xfdesktop-file-utils.c:992
msgid "Could not empty the trash"
msgstr "Não foi possível esvaziar o lixo"

#: ../src/xfdesktop-file-utils.c:1046
msgid "Create File Error"
msgstr "Erro ao criar ficheiro"

#: ../src/xfdesktop-file-utils.c:1047
msgid "Could not create a new file"
msgstr "Não foi possível criar o novo ficheiro"

#: ../src/xfdesktop-file-utils.c:1104
msgid "Create Document Error"
msgstr "Erro ao criar documento"

#: ../src/xfdesktop-file-utils.c:1105
msgid "Could not create a new document from the template"
msgstr "Não foi possível criar o documento com base no modelo"

#: ../src/xfdesktop-file-utils.c:1156
msgid "File Properties Error"
msgstr "Erro nas propriedades do ficheiro"

#: ../src/xfdesktop-file-utils.c:1157
msgid "The file properties dialog could not be opened"
msgstr "Não foi possível abrir a janela de propriedades do ficheiro"

#: ../src/xfdesktop-file-utils.c:1218
msgid "The file could not be opened"
msgstr "Não foi possível abrir o ficheiro"

#: ../src/xfdesktop-file-utils.c:1292 ../src/xfdesktop-file-utils.c:1316
#, c-format
msgid "Failed to run \"%s\""
msgstr "Falha ao executar \"%s\""

#: ../src/xfdesktop-file-utils.c:1382
msgid "The application chooser could not be opened"
msgstr "Não foi possível abrir o seletor de aplicações"

#: ../src/xfdesktop-file-utils.c:1441 ../src/xfdesktop-file-utils.c:1456
#: ../src/xfdesktop-file-utils.c:1519 ../src/xfdesktop-file-utils.c:1536
msgid "Transfer Error"
msgstr "Erro ao transferir"

#: ../src/xfdesktop-file-utils.c:1442 ../src/xfdesktop-file-utils.c:1457
#: ../src/xfdesktop-file-utils.c:1520 ../src/xfdesktop-file-utils.c:1537
msgid "The file transfer could not be performed"
msgstr "Não foi possível transferir o ficheiro"

#. TRANSLATORS: Please use the same translation here as in Thunar
#: ../src/xfdesktop-notify.c:127
msgid "Unmounting device"
msgstr "A desmontar dispositivo"

#. TRANSLATORS: Please use the same translation here as in Thunar
#: ../src/xfdesktop-notify.c:130
#, c-format
msgid ""
"The device \"%s\" is being unmounted by the system. Please do not remove the"
" media or disconnect the drive"
msgstr ""
"O dispositivo \"%s\" está a ser desmontado do sistema. Por favor não remova "
"ou desligue a unidade"

#. TRANSLATORS: Please use the same translation here as in Thunar
#: ../src/xfdesktop-notify.c:137 ../src/xfdesktop-notify.c:322
msgid "Writing data to device"
msgstr "A escrever dados no dispositivo"

#. TRANSLATORS: Please use the same translation here as in Thunar
#: ../src/xfdesktop-notify.c:140 ../src/xfdesktop-notify.c:325
#, c-format
msgid ""
"There is data that needs to be written to the device \"%s\" before it can be"
" removed. Please do not remove the media or disconnect the drive"
msgstr ""
"Existem dados que precisam de ser escritos no dispositivo \"%s\" antes que o"
" possa remover. Por favor não remova ou desligue a unidade"

#: ../src/xfdesktop-notify.c:221
msgid "Unmount Finished"
msgstr "Desmontagem concluída"

#: ../src/xfdesktop-notify.c:223 ../src/xfdesktop-notify.c:408
#, c-format
msgid "The device \"%s\" has been safely removed from the system. "
msgstr "O dispositivo \"%s\" foi removido do sistema de forma segura."

#. TRANSLATORS: Please use the same translation here as in Thunar
#: ../src/xfdesktop-notify.c:313
msgid "Ejecting device"
msgstr "A ejetar dispositivo"

#. TRANSLATORS: Please use the same translation here as in Thunar
#: ../src/xfdesktop-notify.c:316
#, c-format
msgid "The device \"%s\" is being ejected. This may take some time"
msgstr "O dispositivo \"%s\" está a ser ejetado. Pode levar algum tempo"

#: ../src/xfdesktop-notify.c:406
msgid "Eject Finished"
msgstr "Ejeção concluída"

#: ../src/xfdesktop-regular-file-icon.c:817
#, c-format
msgid ""
"Name: %s\n"
"Type: %s\n"
"Size: %s\n"
"Last modified: %s"
msgstr ""
"Nome: %s\n"
"Tipo: %s\n"
"Tamanho: %s\n"
"Última modificação: %s"

#: ../src/xfdesktop-special-file-icon.c:294
#: ../src/xfdesktop-special-file-icon.c:476
msgid "File System"
msgstr "Sistema de ficheiros"

#: ../src/xfdesktop-special-file-icon.c:461
msgid "Trash is empty"
msgstr "O lixo está vazio"

#: ../src/xfdesktop-special-file-icon.c:464
msgid "Trash contains one item"
msgstr "O lixo contém um item"

#: ../src/xfdesktop-special-file-icon.c:465
#, c-format
msgid "Trash contains %d items"
msgstr "O lixo contém %d itens"

#: ../src/xfdesktop-special-file-icon.c:494
#, c-format
msgid ""
"%s\n"
"Size: %s\n"
"Last modified: %s"
msgstr ""
"%s\n"
"Tamanho: %s\n"
"Última modificação: %s"

#: ../src/xfdesktop-special-file-icon.c:561
msgid "_Empty Trash"
msgstr "_Esvaziar lixo"

#: ../src/xfdesktop-volume-icon.c:480
#, c-format
msgid ""
"Removable Volume\n"
"Mounted in \"%s\"\n"
"%s left (%s total)"
msgstr ""
"Dispositivo amovível\n"
"Montado em \"%s\"\n"
"%s disponível (%s total)"

#: ../src/xfdesktop-volume-icon.c:487
msgid ""
"Removable Volume\n"
"Not mounted yet"
msgstr ""
"Dispositivo amovível\n"
"Ainda não montado"

#: ../src/xfdesktop-volume-icon.c:516 ../src/xfdesktop-volume-icon.c:562
#, c-format
msgid "Failed to eject \"%s\""
msgstr "Falha ao ejetar \"%s\""

#: ../src/xfdesktop-volume-icon.c:521 ../src/xfdesktop-volume-icon.c:567
msgid "Eject Failed"
msgstr "Falha ao ejetar"

#: ../src/xfdesktop-volume-icon.c:600
#, c-format
msgid "Failed to mount \"%s\""
msgstr "Falha ao montar \"%s\""

#: ../src/xfdesktop-volume-icon.c:603
msgid "Mount Failed"
msgstr "Falha ao montar"

#: ../src/xfdesktop-volume-icon.c:825
msgid "E_ject Volume"
msgstr "E_jetar volume"

#: ../src/xfdesktop-volume-icon.c:832
msgid "_Unmount Volume"
msgstr "Desmontar vol_ume"

#: ../src/xfdesktop-volume-icon.c:839
msgid "_Mount Volume"
msgstr "_Montar volume"

#: ../src/xfdesktop-window-icon.c:193
msgid "_Window Actions"
msgstr "Ações de ja_nela"
